from manimlib.imports import *
import colorsys

try:                 # height, fps, aspect ratio
    set_custom_quality(700, 15, 8/9)
except:
    pass

class NewColor:
    def __init__(self,v1,v2,v3,color_type="rgb"):
        if color_type == "rgb":
            self.vals = (v1 / 255, v2 / 255, v3 / 255)
        self.color_type = color_type
        self.color = self.get_color_values(self.vals, color_type)

    def get_color(self):
        return self.color 

    def get_color_values(self, vals, color_type):
        return Color(**{f"{color_type}": vals})

    def rgb_to_hls(self):
        return colorsys.rgb_to_hls(*self.vals)

    def more_darker(self, alpha):
        h,l,s = self.rgb_to_hls()
        l = interpolate(l,0,alpha)
        return self.get_color_values((h,s,l),"hsl")

def D_lambda(l=3):
    return np.array([[l, 0], [0, 1]])

def D2_lambda(l):
    pass

def vector_coordinate_label(vector_mob, integer_labels=True,
                            n_dim=2, color=WHITE, apply_extra_method = lambda x: x):
    vect = np.array(vector_mob.get_end())
    if integer_labels:
        vect = np.round(vect).astype(int)
    vect = vect[:n_dim]
    vect = vect.reshape((n_dim, 1))
    label = Matrix(vect, add_background_rectangles_to_entries=False)
    label.scale(1.2)

    shift_dir = np.array(vector_mob.get_end())
    if shift_dir[0] >= 0:  # Pointing right
        shift_dir -= label.get_left() + DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * LEFT
    else:  # Pointing left
        shift_dir -= label.get_right() + DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * RIGHT
    label.shift(shift_dir)
    label.set_color(color)
    apply_extra_method(label)
    label.rect = BackgroundRectangle(label)
    label.add_to_back(label.rect)
    return label

NARANJA_CLASS = NewColor(238, 125, 49)
NARANJA = NARANJA_CLASS.get_color()
NARANJA_TENUE = NARANJA_CLASS.more_darker(0.7)
P = np.array([[-1,-1], [2,1]])
P_1 = np.linalg.inv(P)

P2 = np.array(
    [
        [-1,3],
        [2,1]
    ]
)
P2_1 = np.linalg.inv(P)

class TransformacionLineal(LinearTransformationScene,MovingCameraScene):
    CONFIG = {
        "camera_class": MovingCamera, 
        "include_background_plane": True,
        "include_foreground_plane": True,
        "foreground_plane_kwargs": {
            "x_max": FRAME_WIDTH*3,
            "x_min": -FRAME_WIDTH*3,
            "y_max": FRAME_WIDTH*3,
            "y_min": -FRAME_WIDTH*3,
            "color": NARANJA,
            "background_line_style": {
                "stroke_color": NARANJA,
                "stroke_width": 3,
            },
        },
        "background_plane_kwargs": {
            "color": GRAY,
            "axis_config": {
                "stroke_color": WHITE,
            },
            "background_line_style": {
                "stroke_color": GRAY,
                "stroke_width": 2,
            },
            "x_max": FRAME_WIDTH*2,
            "x_min": -FRAME_WIDTH*2,
            "y_max": FRAME_WIDTH*2,
            "y_min": -FRAME_WIDTH*2,
        },
        "show_coordinates": False,
        "show_basis_vectors": False,
        "basis_vector_stroke_width": 6,
        "i_hat_color": X_COLOR,
        "j_hat_color": Y_COLOR,
        "leave_ghost_vectors": False,
        "start_with_animation": True,
    }

    def pre_setup(self):
        MovingCameraScene.setup(self)
        LinearTransformationScene.setup(self)
        self.camera_frame.scale(0.8)
        self.fwr = self.camera_frame.get_width() / 2
        self.fhr = self.camera_frame.get_height() / 2
        self.frame_rectangle = Rectangle(
                                width=self.fwr * 2,
                                height=self.fhr * 2,
                                fill_opacity=0.5,
                            )
        self.background_plane_axes = self.background_plane.get_axes()
        self.plane_axes = self.plane.get_axes()

    def remove_grids(self):
        fade_objects = [self.background_plane.faded_lines,
            self.background_plane.background_lines,
            self.plane]
        self.play(*list(map(FadeOut,fade_objects)))

    def mob_to_corner(self,mob,direction,buff=0.5):
        position = self.frame_rectangle.get_corner(direction)
        mob.next_to(position, - direction, buff=buff)

class Escena1(TransformacionLineal):
    def construct(self):
        self.pre_setup()
        dot_center = Dot(color=RED,radius=0.12)
        if self.start_with_animation:
            self.play(
                *list(map(ShowCreation,[self.background_plane, self.plane])),
                GrowFromCenter(dot_center)
                )
        self.add_foreground_mobject(dot_center)
        self.wait()
        self.definiciones()
        self.parte_matriz()
        self.parte_circunferencia()

    def parte_matriz(self,wt=1.2):
        M_1 = self.M_1
        textos = self.textos
        matrix_mob = IntegerMatrix(M_1).scale(1.5)
        matrix_mob.add_to_back(BackgroundRectangle(matrix_mob))
        matrix_mob.next_to(ORIGIN,DOWN)
        vu_i = Vector(RIGHT, color=X_COLOR)
        vu_j = Vector(UP ,color=Y_COLOR)
        _vu_i_t = Vector([-1,4,0], color=X_COLOR)
        _vu_j_t = Vector([-2,5,0], color=Y_COLOR)
        vu_i_t = vector_coordinate_label(_vu_i_t).next_to([-1,4,0],RIGHT,buff=0.3)
        vu_i_t[1:].set_color(X_COLOR)
        vu_j_t = vector_coordinate_label(_vu_j_t).next_to([-2,5,0],LEFT,buff=0.3)
        vu_j_t[1:].set_color(Y_COLOR)
        self.mob_to_corner(textos,DL)
        self.apply_matrix(M_1)
        self.wait(wt)
        self.play(Write(textos[0]))
        self.wait(wt)
        self.play(Write(textos[1]))
        self.wait(wt)
        self.resetear()
        self.play(FadeOut(textos))
        self.wait(wt)
        self.add_vector(vu_i)
        vu_i_c = self.write_vector_coordinates(vu_i,apply_extra_method=lambda x: x.set_color(X_COLOR))
        self.add_vector(vu_j)
        vu_j_c = self.write_vector_coordinates(vu_j,
            apply_extra_method=lambda x: x.next_to(vu_j.get_end(),LEFT).set_color(Y_COLOR)
        )
        self.wait(wt)
        self.apply_matrix(
            M_1,
            added_anims=[
                ReplacementTransform(vu_i_c,vu_i_t),
                ReplacementTransform(vu_j_c,vu_j_t),
            ]
        )

        numbers_t = VGroup(*vu_i_t[1],*vu_j_t[1])
        matrix_mob_numbers = VGroup(
            matrix_mob[1][0],matrix_mob[1][2],
            matrix_mob[1][1],matrix_mob[1][3]
            )
        matrix_mob_numbers[:2].set_color(X_COLOR)
        matrix_mob_numbers[2:].set_color(Y_COLOR)
        matrix_mob_without_numbers = VGroup(matrix_mob[0],matrix_mob[2:])
        m_label = TexMobject("M=").scale(1.5)
        m_label.next_to(matrix_mob_without_numbers,LEFT,buff=0.35)

        self.play(Write(matrix_mob_without_numbers))
        self.play(*[
                ReplacementTransform(pre_num[0][:],post_num[:],run_time=3)
                for pre_num,post_num in zip(numbers_t,matrix_mob_numbers)
                ],
                Write(m_label),
                *list(map(
                        lambda mob: FadeOut(mob,run_time=3),
                        [vu_i_t[0],vu_j_t[0],vu_i_t[2:],vu_j_t[2:]]
                ))
        )
        self.wait(wt)
        self.play(*list(map(FadeOut,[matrix_mob,m_label])))
        self.wait(wt)
        self.resetear()
        self.unit_vectors = VGroup(vu_i,vu_j)

    def parte_circunferencia(self,wt=1.2):
        circulo = Circle(radius=1,color=NARANJA)
        self.play(ShowCreation(circulo))
        self.wait(wt)
        self.add_transformable_mobject(circulo)
        self.apply_matrix(self.M_1)
        self.wait(wt)
        self.play(*[
            FadeOut(mob) for mob in [
                                    self.unit_vectors, 
                                    self.plane,
                                    self.background_plane.faded_lines,
                                    self.background_plane.background_lines
                                    ]
        ])
        self.wait(wt)
        self.moving_vectors = []
        self.plane.set_stroke(opacity=0)
        self.resetear()
        self.wait(wt)
        circulo.fade(1)
        self.remove(circulo)

    def write_vector_coordinates(self, vector, **kwargs):
        coords = vector_coordinate_label(vector, **kwargs)
        self.play(Write(coords))
        self.add_foreground_mobject(coords)
        return coords

    def add_vector(self, vector, color=YELLOW, animate=True, added_anims=[],**kwargs):
        if not isinstance(vector, Arrow):
            vector = Vector(vector, color=color, **kwargs)
        if animate:
            self.play(GrowArrow(vector),*added_anims)
        self.add(vector)
        self.moving_vectors.append(vector)
        return vector

    def definiciones(self):
        self.M_1 = reduce(np.matmul, [P, D_lambda(), P_1])
        self.textos = VGroup(
            TextMobject("El origen no se mueve"),
            TextMobject("Líneas paralelas")
        ).scale(1.5)
        self.textos.arrange(DOWN)

    def resetear(self):
        self.apply_matrix(np.linalg.inv(self.M_1))

class Escena2(Escena1,MovingCameraScene):
    CONFIG = {
        "general_wait_time": 2,
        "camera_class": MovingCamera,
    }

    def construct(self):
        self.M_1 = reduce(np.matmul, [P2, D_lambda(), P2_1])
        #print(self.M_1)
        Escena1.definiciones(self)
        Escena1.construct(self)
        #MovingCameraScene.setup(self)
        self.wait()
        self.play(
            self.camera_frame.scale,2
        )
        self.wait()
        #self.remove_grids()
        self.definiciones_2()
        self.aplicar_M_vectores()

    def aplicar_M_vectores(self):
        vu_i = Vector(RIGHT, color=X_COLOR)
        vu_j = Vector(UP ,color=Y_COLOR)
        vectores_extra = VGroup(*[
            Vector(direction, color=YELLOW)
            for direction in [(1/2,np.sqrt(3)/2,0),(np.sqrt(3)/2,-1/2,0)]
        ])
        lineas_vectores_extra = self.get_lines_from_vectors(
                                                vectores_extra,
                                                color=YELLOW_D,
                                                stroke_width=1.5
        )
        veps = VGroup(*[
            Vector(
                (direction[0]/get_norm(direction),
                direction[1]/get_norm(direction),direction[2]/get_norm(direction)), color=NARANJA)
            for direction in [(-1,2,0),(-3,1,0)]
        ])
        vectores_unitarios = VGroup(vu_i,vu_j)
        self.add_vector(vu_i)
        self.wait()
        self.add_vector(vu_j)
        self.wait()
        for vect,lin in zip(vectores_extra,lineas_vectores_extra):
            self.add_vector(vect,added_anims=[ShowCreation(lin)])
            self.wait()
        self.aplicar_transformacion()
        self.wait()
        self.resetear()
        self.play(
            *list(map(FadeOut,[vectores_unitarios,vectores_extra,lineas_vectores_extra])),
            *list(map(GrowArrow,veps))
        )
        self.moving_vectors = []
        veps_lineas = self.get_lines_from_vectors(veps, color=NARANJA, stroke_width=1.5)
        self.wait()
        self.play(*[
            TransformFromCopy(vector,line)
            for vector,line in zip(veps,veps_lineas)
        ])
        self.wait()
        self.circulo.save_state()
        veps.save_state()
        vector = [-3,6,0]
        u_vector = [-3*3/get_norm(vector),6*3/get_norm(vector),0]
        v_vector = [-3*(-7)/get_norm([3,1,0]),1*(-7)/get_norm([3,1,0]),0]
        self.play(
            Transform(veps[0],Vector(u_vector,color=NARANJA),run_time=3),
            Transform(veps[1],Vector(v_vector,color=NARANJA),run_time=3),
            #ApplyMatrix(reduce(np.matmul, [P2, D_lambda(), P2_1]),veps[1],run_time=3),
            ApplyMatrix(reduce(np.matmul, [P2, D_lambda(), P2_1]),self.circulo,run_time=3),
        )
        brace_1 = self.get_brace_from_vector(veps[0],buff=0.1)
        brace_1_tex = brace_1.get_tex("\\times 3")
        brace_2 = self.get_brace_from_vector(veps[1],sign=-1,buff=0.1)
        brace_2_tex = brace_2.get_tex("\\times -7")
        self.play(GrowFromCenter(brace_1),Write(brace_1_tex))
        self.wait()
        self.play(GrowFromCenter(brace_2),Write(brace_2_tex))
        self.wait()
        self.play(*list(map(FadeOut,[brace_1,brace_2,brace_1_tex,brace_2_tex])))
        self.play(*list(map(Restore,[self.circulo,veps])))
        self.play(FadeOut(self.circulo))
        self.wait()
        veps_trans = VGroup(*[
            Vector(direction, color=NARANJA)
            for direction in [(-3*3/get_norm(vector),6*3/get_norm(vector),0),(-3*(-7)/get_norm([3,1,0]),1*(-7)/get_norm([3,1,0]),0)]
        ])
        brace_1 = self.get_brace_from_vector(
            veps_trans[0],
            "\\lambda_1\\cdot v_1",buff=0.1
        )
        brace_2 = self.get_brace_from_vector(
            veps_trans[1],
            "\\lambda_2\\cdot v_2",sign=-1,buff=0.1
        )
        for mob in [brace_1,brace_2]:
            mob.tex.scale(1.3)
        self.play(
            GrowFromCenter(brace_1),Write(brace_1.tex.shift(RIGHT * 0.1)),
            GrowFromCenter(brace_2),Write(brace_2.tex),
            *[Transform(v,vt) for v,vt in zip(veps,veps_trans)]
        )
        self.wait()
        self.play(
            *list(map(FadeOut,[
                veps_lineas,brace_1,brace_1.tex,brace_2,brace_2.tex
            ]))
        )
        self.wait()
        self.veps_escena_2 = veps

    def wait(self,wt=0.5):
        Escena1.wait(self,self.general_wait_time)

    def aplicar_transformacion(self,more_anims=[],**kwargs):
        self.apply_matrix(reduce(np.matmul, [P2, D_lambda(), P2_1]),added_anims=more_anims,**kwargs)

    def get_lines_from_vectors(self,vectors,**kwargs):
        return VGroup(*[
            Line(
                - vector.get_unit_vector() * 10,
                vector.get_unit_vector() * 10,
                **kwargs
            )
            for vector in vectors
        ])

    def get_brace_from_vector(self,mob,tex="x",sign=-1,tex_kwargs={},**kwargs):
        brace = Brace(mob,direction=mob.copy().rotate(sign*PI/2).get_unit_vector(),**kwargs)
        brace.tex = brace.get_tex(tex,**tex_kwargs)
        return brace

    def definiciones_2(self):
        self.M_1 = reduce(np.matmul, [P2, D_lambda(), P2_1])
        self.circulo = Circle(radius=1,color=NARANJA)
        self.add_transformable_mobject(self.circulo)


class Escena3(Escena2,Escena1,MovingCameraScene):
    CONFIG = {
        "general_wait_time": 1.5,
        "camera_class": MovingCamera,
    }
    def pre_setup(self):
        self.fwr = self.camera_frame.get_width() / 2
        self.fhr = self.camera_frame.get_height() / 2
        self.frame_rectangle = Rectangle(
                                width=self.fwr * 2,
                                height=self.fhr * 4,
                                fill_opacity=0.5,
                            )
        Escena1.setup(self)
        self.veps = self.get_veps()
        for vep in self.veps:
            self.add_vector(vep,animate=False)
        self.plane.save_state()
        self.veps[0].save_state()
        self.veps[1].save_state()

    def construct(self):
        # lambda = -3
        MovingCameraScene.setup(self)
        self.fwr = self.camera_frame.get_width() / 2
        self.fhr = self.camera_frame.get_height() / 2
        self.frame_rectangle = Rectangle(
                                width=self.fwr * 2,
                                height=self.fhr * 4,
                                fill_opacity=0.5,
                            )
        self.veps = self.get_veps()
        for vep in self.veps:
            self.add_vector(vep,animate=False)
        self.plane.save_state()
        self.veps[0].save_state()
        self.veps[1].save_state()
        self.wait()
        #self.play(self.camera_frame.scale,1/2)
        #self.wait()
        self.single_animation(
            l=-3,times="-3",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.3)
            }
        )
        # lambda = - 1 / 2
        self.single_animation(
            l=-0.5,times="-\\frac{1}{2}",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.4)
            }
        )
        # lambda = 0
        self.single_animation(
            l=0,times="0",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.3)
            }
        )
        vu_i = Vector(RIGHT, color=X_COLOR)
        vu_j = Vector(UP ,color=Y_COLOR)
        self.add_vector(vu_i)
        self.add_vector(vu_j)
        self.aplicar_transformacion()
        self.wait()



    def single_animation(self,l,times,show_brace_kwargs={}):
        self.Mat = self.get_M(l)
        label = TextMobject(f"Aplicamos $\\displaystyle M\\left(\\lambda={times}\\right)$").scale(2)
        self.wait()
        self.mob_to_corner(label,UR)
        self.play(Write(label))
        self.wait()
        self.aplicar_transformacion()
        self.show_brace(self.veps[0],f"\\times {times}",**show_brace_kwargs)
        self.wait()
        self.resetear(more_anims=[FadeOut(label)])
        self.wait()


    def get_veps(self):
        return VGroup(*[
            Vector(direction, color=NARANJA)
            for direction in [(-1,2,0),(-3,1,0)]
        ])

    def get_M(self,l):
        return reduce(np.matmul, [P2, D_lambda(l), P2_1])

    def aplicar_transformacion(self,mat=None,more_anims=[],**kwargs):
        if mat == None:
            mat = self.Mat
        self.apply_matrix(mat,added_anims=more_anims,**kwargs)

    def resetear(self,more_anims=[],**kwargs):
        try:
            self.apply_matrix(np.linalg.inv(self.Mat),added_anims=more_anims,**kwargs)
        except:
            self.play(Restore(self.plane),Restore(self.veps[0]),Restore(self.veps[1]),*more_anims)

    def show_brace(self,vect,times,**kwargs):
        brace = self.get_brace_from_vector(vect,times,**kwargs)
        self.play(
            GrowFromCenter(brace),
            Write(brace.tex)
        )
        self.wait()
        self.play(*list(map(FadeOut,[brace,brace.tex])))

    def get_brace_from_vector(self,
                              mob,
                              tex="x",
                              sign=-1,
                              tex_kwargs={},
                              tex_func=lambda x: x,
                              **kwargs):
        brace = Brace(mob,direction=mob.copy().rotate(sign*PI/2).get_unit_vector(),**kwargs)
        brace.tex = brace.get_tex(tex,**tex_kwargs)
        tex_func(brace.tex)
        background = BackgroundRectangle(brace.tex)
        brace.tex.add_to_back(background)
        return brace

class EscenaFull(Escena2,MovingCameraScene):
    CONFIG = {
        "general_wait_time": 1.5,
        "camera_class": MovingCamera,
    }
    def construct(self):
        Escena2.construct(self)
        MovingCameraScene.setup(self)
        self.wait()
        self.veps = self.get_veps()
        self.play(ReplacementTransform(self.veps_escena_2,self.veps))
        self.transformable_mobjects = []
        for vep in self.veps:
            self.add_vector(vep,animate=False)
        self.play(
            self.background_plane.set_stroke,None,None,1,
            self.plane.set_stroke,None,None,1,
        )
        self.play(self.camera_frame.scale,1/2)
        self.add_transformable_mobject(self.plane)
        self.add_background_mobject(self.background_plane)
        self.plane.save_state()
        self.veps.save_state()
        # lambda = -3
        self.single_animation(
            l=-3,times="-3",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.3)
            }
        )
        # lambda = - 1 / 2
        self.single_animation(
            l=-0.5,times="-\\frac{1}{2}",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.4)
            }
        )
        # lambda = 0
        self.single_animation(
            l=0,times="0",
            show_brace_kwargs={
                "tex_kwargs": {"buff":0.3},
                "tex_func": lambda tex: tex.scale(1.3)
            }
        )
        vu_i = Vector(RIGHT, color=X_COLOR)
        vu_j = Vector(UP ,color=Y_COLOR)
        self.add_vector(vu_i)
        self.add_vector(vu_j)
        self.wait()
        self.aplicar_transformacion_2()
        self.wait()



    def single_animation(self,l,times,show_brace_kwargs={}):
        self.Mat = self.get_M(l)
        label = TextMobject(f"Aplicamos $\\displaystyle M\\left(\\lambda={times}\\right)$").scale(2)
        self.wait()
        self.mob_to_corner(label,UR)
        self.play(Write(label))
        self.wait()
        self.aplicar_transformacion_2()
        self.show_brace(self.veps[0],f"\\times {times}",**show_brace_kwargs)
        self.wait()
        self.resetear_2(more_anims=[FadeOut(label)])
        self.wait()


    def get_veps(self):
        return VGroup(*[
            Vector(direction, color=NARANJA)
            for direction in [(-1,2,0)]
        ])

    def get_M(self,l):
        return reduce(np.matmul, [P2, D_lambda(l), P2_1])

    def aplicar_transformacion_2(self,mat=None,more_anims=[],**kwargs):
        if mat == None:
            mat = self.Mat
        self.apply_matrix(mat,added_anims=more_anims,**kwargs)
        print(self.Mat)

    def resetear_2(self,more_anims=[],**kwargs):
        try:
            self.apply_matrix(np.linalg.inv(self.Mat),added_anims=more_anims,**kwargs)
            print(self.Mat)
        except:
            self.play(Restore(self.plane),Restore(self.veps),*more_anims)

    def show_brace(self,vect,times,**kwargs):
        brace = self.get_brace_from_vector_2(vect,times,**kwargs)
        self.play(
            GrowFromCenter(brace),
            Write(brace.tex)
        )
        self.wait()
        self.play(*list(map(FadeOut,[brace,brace.tex])))

    def get_brace_from_vector_2(self,
                              mob,
                              tex="x",
                              sign=-1,
                              tex_kwargs={},
                              tex_func=lambda x: x,
                              **kwargs):
        brace = Brace(mob,direction=mob.copy().rotate(sign*PI/2).get_unit_vector(),**kwargs)
        brace.tex = brace.get_tex(tex,**tex_kwargs)
        tex_func(brace.tex)
        background = BackgroundRectangle(brace.tex)
        brace.tex.add_to_back(background)
        return brace

